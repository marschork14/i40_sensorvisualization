sap.ui.define([
	"mosbach/dhbw/i40_sensorVisualization/controller/BaseController",
	"sap/m/MessageToast"
], function(BaseController, MessageToast) {
	"use strict";

	return BaseController.extend("mosbach.dhbw.i40_sensorVisualization.controller.Settings", {

		/**
		 * Method for handling a press event of the home button
		 * Overwrite method from BaseController
		 */
		onHomePressed: function() {
			this._clearDetailPage();
			this._getRouter().navTo("");
		},

		/**
		 * Convenience method for accessing the views split app
		 */
		getSplitAppObj: function() {
			return this.byId("SplitApp");
		},
		
		/**
		 * Method for handling press events on the machine master's navigation button
		 */
		onPressToMachines: function() {
			this._clearDetailPage();
		},
		
		/**
		 * Method for handling press events on machine items 
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if a machine item is selected
		 */
		onMachinePressed: function(oEvent) {
			var sPath = oEvent.getSource().getBindingContext("machine").getPath();
			var nMachineID = this._getModel("machine").getProperty(sPath).machineID;
			var oThis = this;
			jQuery.ajax({
				type: "GET",
				contentType: "application/json",
				url: "/rest/sensor",
				data: {
					machineID: parseInt(nMachineID, 10)
				},
				dataType: "json",
				error: function(oError) {
					oThis._createDialog("titleXS", "messageXS");
				},
				success: function(oData) {
					oThis._getModel("sensor").setData({
						sensors: oData
					});
				}
			});
			this.getSplitAppObj().toMaster(this.createId("masterSensor"));
		},
		
		/**
		 * Method for handling press events on sensor items 
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if a sensor item is selected
		 */
		onSensorPressed: function(oEvent) {
			var sPath = oEvent.getSource().getBindingContext("sensor").getPath();
			var oSensor = this._getModel("sensor").getProperty(sPath);
			this._getModel("sensorDetail").setData(oSensor);
			this.byId("settingsTabBar").setVisible(true);
		},
		
		/**
		 * Method for handling a checkbox selection event for enabling/disabling email notifications
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if the checkbox is selected/unselected
		 */
		onEmailNotificationStateChanged: function(oEvent) {
			var oSensorDetailModel = this._getModel("sensorDetail");
			var sSelected = oEvent.getSource().getSelected() ? "true" : "false";
			oSensorDetailModel.setProperty("/checkThresholdMail", sSelected);
		},

		/**
		 * Method for handling a checkbox selection event for enabling/disabling twitter notifications
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if the checkbox is selected/unselected
		 */
		onTwitterNotificationStateChanged: function(oEvent) {
			var oSensorDetailModel = this._getModel("sensorDetail");
			var sSelected = oEvent.getSource().getSelected() ? "true" : "false";
			oSensorDetailModel.setProperty("/checkThresholdTwitter", sSelected);
		},
		
		/**
		 * Method for a press event of the save button
		 */
		onSavePressed: function() {
			var oThis = this;
			var oInputEmail = this.byId("email");
			var oResourceBundle = this._getResourceBundle();
			var oSensor = this._getModel("sensorDetail").getProperty("/");
			//check if email format is valid, but only if email notification is enabled
			if ((oInputEmail.getValueState() === "Error" || oInputEmail.getValue().length === 0) && oSensor.checkThresholdMail === "true") {
				this._createDialog("wrongEmail", "wrongInputEmail");
			//check if thresholds are valid 
			} else if (oSensor.thresholdLow > oSensor.thresholdHigh && oSensor.sensorType !== 'b') {
				this._createDialog("wrongThreshold", "wrongInputThreshold");
			//valid input
			} else {
				//sensors of type boolean only use the thresholdLow attribute
				if (oSensor.sensorType === "b") {
					var bSwitcherState = this._byId("stateSwitcher").getState();
					oSensor.thresholdLow = (bSwitcherState ? 1 : 0);
				}
				jQuery.ajax({
					type: "PUT",
					contentType: "application/json",
					url: "/rest/sensor",
					data: JSON.stringify(oSensor),
					dataType: "json",
					error: function(oError) {
						oThis._createDialog("titleXS", "messageXS");
					},
					success: function(oData) {
						MessageToast.show(oResourceBundle.getText("successfulSensorUpdate"));
					}
				});
			}
		},
		
		/**
		 * Method for handling a liveSearch event of the machine search bar
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if the value of search bar changes
		 */
		onMachineSearch: function(oEvent) {
			var aFilters = this._createFilter(oEvent);
			var list = this.getView().byId("machineList");
			list.getBinding("items").filter(aFilters);
		},
		
		/**
		 * Method for handling a liveSearch event of the machine search bar
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if the value of search bar changes
		 */
		onSensorSearch: function(oEvent) {
			var aFilters = this._createFilter(oEvent);
			var list = this.getView().byId("sensorList");
			list.getBinding("items").filter(aFilters);
		},
		
		/**
		 * Helper method for creating a filter from a liveChange event of a search bar
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if the value of a search bar changes
		 * @returns {array} array of filters
		 */
		_createFilter: function(oEvent) {
			// add filter for search
			var aFilters = [];
			var sQuery = oEvent.getSource().getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = new sap.ui.model.Filter("name", sap.ui.model.FilterOperator.Contains, sQuery);
				aFilters.push(filter);
			}
			return aFilters;
		},
		
		/**
		 * Helper method for clearing the split apps detail page and navigating to the top level master page
		 */
		_clearDetailPage: function() {
			this.byId("settingsTabBar").setVisible(false);
			this.byId("detailPage").setTitle("");
			this.getSplitAppObj().toMaster(this.createId("masterMachine"));
		}
	});
});