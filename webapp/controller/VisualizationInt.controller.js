sap.ui.define([
	"mosbach/dhbw/i40_sensorVisualization/controller/Visualization"
], function(Visualization) {
	"use strict";

	return Visualization.extend("mosbach.dhbw.i40_sensorVisualization.controller.VisualizationInt", {
		
		/**
		 * Method for initializing the controller
		 */
		onInit: function() {
			this._getRouter().getRoute("visualizationInt").attachPatternMatched(this._onObjectMatched, this);
		},
		
		/**
		 * Method for making an OData request to the MMS and handling the result
		 * @param {sap.ui.core.mvc.Controller} [oController] controller of the current view
		 */
		_getSensorData: function(oController) {
			var oODataModel = oController._getModel("odata");
			var oSensorUiModel = oController._getModel("sensorUiInt");
			var sTechnicalSensorName = oController._getModel("sensorDetail").getProperty("/technicalName");
			var oTimeNow = new Date();
			var nIntervalMinute = Number(oSensorUiModel.getProperty("/intervalMinute"));
			var nIntervalHour = Number(oSensorUiModel.getProperty("/intervalHour"));
			//(minutes * seconds/minute)+hour*seconds/hour)*milliseconds/second
			var oTimeStart = new Date(oTimeNow.getTime() - (nIntervalMinute * 60 + nIntervalHour * 3600) * 1000);
			oODataModel.read(oController._getModel("config").getProperty("/tableName"), {
				filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", oTimeStart, oTimeNow), new sap.ui.model.Filter("C_SENSORNAME", "EQ",
					sTechnicalSensorName)],
				sorters: [new sap.ui.model.Sorter("C_TIMESTAMP", true)],
				success: function(oData) {
					if (oData.results.length > 0) {
						var oCalcValues = oController._calculateValues(oData.results);
						oController._setModelProperties("sensorUiInt", ["current", "minimum", "maximum"], oCalcValues);
						oController._checkValues("sensorUiInt", ["current", "minimum", "maximum"]);
						oController._checkThreshold(oCalcValues.minMeasurement, oCalcValues.maxMeasurement);
					} else {
						oController._resetTileContentColor(["current", "minimum", "maximum"]);
					}
				}
			});
		},
		
		/**
		 * Method for handling press events on the show button in the diagram tab
		 */
		onShowData: function() {
			this._onShowData("column", false);
		},
		
		/**
		 * Helper method for calculating different values for the requested data set, e.g. minmum and maximum
		 * @param {array} [aMeasurements] array of data points
		 * @return {object} object containing the different calculated values and corresponding timestamps
		 */
		_calculateValues: function(aMeasurements) {
			if (aMeasurements.length === 0) {
				return {
					current: 0,
					minimum: 0,
					maximum: 0
				};
			}
			var oMinimum;
			var oMaximum;
			var nMin = Number.MAX_VALUE;
			var nMax = Number.MIN_VALUE;
			jQuery.each(aMeasurements, function(nIndex, oMeasurement) {
				var nValue = Number(oMeasurement.C_VALUE);
				if (nValue < nMin) {
					nMin = nValue;
					oMinimum = oMeasurement;
				}
				if (nValue > nMax) {
					nMax = nValue;
					oMaximum = oMeasurement;
				}
			});
			return {
				current: Number(aMeasurements[0].C_VALUE),
				minimum: nMin,
				maximum: nMax,
				minMeasurement: oMinimum,
				maxMeasurement: oMaximum
			};
		}
	});
});