sap.ui.define([
	"mosbach/dhbw/i40_sensorVisualization/controller/Visualization"
], function(Visualization) {
	"use strict";

	return Visualization.extend("mosbach.dhbw.i40_sensorVisualization.controller.VisualizationBool", {
		
		/**
		 * Method for initializing the controller
		 */
		onInit: function() {
			this._getRouter().getRoute("visualizationBool").attachPatternMatched(this._onObjectMatched, this);
		},
		
		/**
		 * Method for making an OData request to the MMS and handling the result
		 * @param {sap.ui.core.mvc.Controller} [oController] controller of the current view
		 */
		_getSensorData: function(oController) {
			var oODataModel = oController._getModel("odata");
			var oSensorUiModel = oController._getModel("sensorUiBool");
			var sTechnicalSensorName = oController._getModel("sensorDetail").getProperty("/technicalName");
			var oTimeNow = new Date();
			var nIntervalSecond = Number(oSensorUiModel.getProperty("/intervalSecond"));
			var nIntervalMinute = Number(oSensorUiModel.getProperty("/intervalMinute"));
			//(minutes * seconds/minute) + seconds) * milliseconds/second
			var oTimeStart = new Date(oTimeNow.getTime() - ((nIntervalMinute * 60) + nIntervalSecond) * 1000);
			oODataModel.read(oController._getModel("config").getProperty("/tableName"), {
				filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", oTimeStart, oTimeNow), new sap.ui.model.Filter("C_SENSORNAME", "EQ",
					sTechnicalSensorName)],
				sorters: [new sap.ui.model.Sorter("C_TIMESTAMP", true)],
				success: function(oData) {
					if (oData.results.length > 0) {
						var oCalcValues = oController._calculateValues(oData.results);
						oController._setModelProperties("sensorUiBool", ["current"], oCalcValues);
						oController._checkValues(oCalcValues.current);
						oController._checkThresholdBool(oCalcValues);
					} else {
						oController._resetTileContentColor(["current"]);
					}
				}
			});
		},
		
		/**
		 * Method for handling press events on the show button in the diagram tab
		 */
		onShowData: function() {
			this._onShowData("line", true);
		},
		
		/**
		 * Helper method for calculating current value for the requested data set
		 * @param {array} [aMeasurements] array of data points
		 * @return {object} object containing the different calculated values and corresponding timestamps
		 */
		_calculateValues: function(aMeasurements) {
			if (aMeasurements.length === 0) {
				return {
					current: null,
					timeStamp: null
				};
			}
			var currentValue;
			if (aMeasurements[0].C_VALUE === "True") {
				currentValue = "high";
			} else {
				currentValue = "low";
			}
			return {
				current: currentValue,
				timestamp: aMeasurements[0].C_TIMESTAMP 
			};
		},
		
		/**
		 * Method for setting the value color of the current tile dependend on the specified value
		 * @param {string} [sValue] value of the current measurement ("low" or "high")
		 */
		_checkValues: function(sValue) {
			var bCriticalValue = (this._getModel("sensorDetail").getProperty("/thresholdLow") === 1);
			var sCriticalValue = bCriticalValue ? "high" : "low"; 
			if (sValue === sCriticalValue) {
				this.byId("currentTile").setValueColor("Error");
			} else {
				this.byId("currentTile").setValueColor("Good");
			}
		},
		
		/**
		 * Method for checking the thresholds and sending notifications if enabeled
		 * @param {object} [oCalcValue] object containing the value and timestamp of the current measurement
		 */
		_checkThresholdBool: function(oCalcValue) {
			var sValue = oCalcValue.current;
			var oSensorDetail = this._getModel("sensorDetail");
			var bCheckMail = (oSensorDetail.getProperty("/checkThresholdMail") === "true");
			var bCheckTwitter = (oSensorDetail.getProperty("/checkThresholdTwitter") === "true");
			var bIsCritical = (oSensorDetail.getProperty("/thresholdExceeded") === "true");
			//email and/or twitter notification is enabled
			if (bCheckMail || bCheckTwitter) {
				var sCriticalState = (oSensorDetail.getProperty("/thresholdLow") === 1 ? "high" : "low");
				//sensor state is critical and last measurement was okay
				if (sCriticalState === sValue && !bIsCritical) {
					//save critical state of the sensor
					this._updateSensor(true);
					//send email notification if enabled
					if (bCheckMail) {
						this._sendMailBool(oCalcValue, true);
					}
					//send twitter notification if enabled
					if (bCheckTwitter) {
						this._postTweetBool(true);
					}
					return;
				}
				//sensor state is not critical but last measurement was critical
				if ( sCriticalState !== sValue && bIsCritical) {
					//save non-critical state of the sensor
					this._updateSensor(false);
					//send email notification if enabled
					if (bCheckMail) {
						this._sendMailBool(oCalcValue, false);
					}
					//send twitter notification if enabled
					if (bCheckTwitter) {
						this._postTweetBool(false);
					}
				}
			}
		},
		
		/**
		 * Helper method for sending an email via the emailjs service
		 * @param {object} [oCalcValue] object containing the value and timestamp of the current measurement
		 * @param {boolean} [bIsCritical] flag which indicates if the sensor state is critical or not
		 */
		_sendMailBool: function(oCalcValue, bIsCritical) {
			var oData = this._buildMessageBool(oCalcValue,bIsCritical);
			emailjs.send("smtp_server", "boolsensor", oData);
		},
		/**
		 * Helper method for building the email message
		 * @param {object} [oCalcValue] object containing the value and timestamp of the current measurement
		 * @param {boolean} [bIsCritical] flag which indicates if the sensor state is critical or not
		 * @return {object} the email message
		 */
		_buildMessageBool: function(oCalcValue, bIsCritical) {
			var sMachine = this._getMachineNameById(this._getModel("sensorDetail").getProperty("/machine.machineID"));
			var sSensor =  this._getModel("sensorDetail").getProperty("/name");
			var sText;
			var sSubject;
			if (bIsCritical) {
				sSubject = "Kritischer Zustand: " +  sMachine;
				sText = sMachine + " (" + sSensor + ") im kritischen Zustand!";
			} else {
				sSubject = "Sollzustand erreicht: " + sMachine;
				sText = sMachine + " (" + sSensor + ") im Sollzustand!";
			}
			var oSensorDetail = this._getModel("sensorDetail");
			return {
				receiver: oSensorDetail.getProperty("/email"),
				machine: this._getMachineNameById(oSensorDetail.getProperty("/machine.machineID")),
				timestamp: sap.ui.core.format.DateFormat.getDateTimeInstance({
					style: "long"
				}).format(oCalcValue.timestamp),
				subject: sSubject,
				text: sText
			};
		},
		
		/**
		 * Helper method for making a tweet via the codebird-API
		 * @param {boolean} [bIsCritical] flag which indicates if the sensor state is critical or not
		 */
		_postTweetBool: function(bIsCritical) {
			var sMachine = this._getMachineNameById(this._getModel("sensorDetail").getProperty("/machine.machineID"));
			var sSensor =  this._getModel("sensorDetail").getProperty("/name");
			var timestamp = sap.ui.core.format.DateFormat.getDateTimeInstance({
							style: "long"}).format(new Date());
			var params = {
				status: ""
			};
			if (bIsCritical) {
				params.status = sMachine + " (" + sSensor + ") am " + timestamp + " im kritischen Zustand! #hilfe";
			} else {
				params.status = sMachine + " (" + sSensor + ") am " + timestamp + " im Sollzustand! #alleswirdgut";
			}
			cb.__call(
				"statuses_update",
				params,
				function(reply) {
			});
		}
	});
});