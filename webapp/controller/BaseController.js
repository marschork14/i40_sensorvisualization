sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function(Controller, History) {
	"use strict";

	return Controller.extend("mosbach.dhbw.i40_sensorVisualization.controller.BaseController", {

		/**
		 * Convenience method for navigating to the last displayed view if possible
		 * Otherwise navigate to the main menu
		 */
		onNavBackPressed: function() {
			if (History.getInstance().getPreviousHash() !== undefined) {
				history.go(-1);
			} else {
				this._getRouter().navTo("");
			}
		},

		/**
		 * Method for handling a press event of the home button
		 */
		onHomePressed: function() {
			//navigate to main menu
			this._getRouter().navTo("");
		},

		/**
		 * Convenience accessing a views element by the element's id
		 * @param {string} [sId] the id of the element to retrieve
		 * @returns {sap.ui.core.Element}
		 */
		_byId: function(sId) {
			return this.getView().byId(sId);
		},
		/**
		 * Convenience method for accessing the router.
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		_getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		/**
		 * Convenience method for getting the view model by name.
		 * @param {string} [sName] the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		_getModel: function(sName) {
			return this.getView().getModel(sName);
		},

		/**
		 * Convenience method for setting the view model.
		 * @param {sap.ui.model.Model} [oModel] the model instance
		 * @param {string} [sName] the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		_setModel: function(oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		/**
		 * Getter for the resource bundle.
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		_getResourceBundle: function() {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		/**
		 * Convenience method for creating an error dialog
		 * @param {string} [sTitle] reference to the title in the i18n model
		 * @param {string} [sText] reference to the text message in the i18n model
		 */
		_createDialog: function(sTitle, sText) {
			var oBundle = this._getResourceBundle();
			var errorDialog = new sap.m.Dialog({
				title: oBundle.getText(sTitle),
				type: "Message",
				state: "Error",
				content: new sap.m.Text({
					text: oBundle.getText(sText)
				}),
				beginButton: new sap.m.Button({
					text: oBundle.getText("okButton"),
					press: function() {
						errorDialog.close();
					}
				}),
				afterClose: function() {
					errorDialog.destroy();
				}
			});
			errorDialog.open();
		},
		
		/**
		 * Helper method for getting the name of a machine by its id
		 * @param {string} [sID] the id of the machine
		 * @return {string} the name of the machine
		 */
		_getMachineNameById: function(sID) {
			var aMachines = this._getModel("machine").getProperty("/machines");
			for (var i = 0; i < aMachines.length; i++) {
				if (aMachines[i].machineID === sID) {
					return aMachines[i].name;
				}
			}
		}
	});
});