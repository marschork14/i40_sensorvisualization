sap.ui.define([
	"mosbach/dhbw/i40_sensorVisualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("mosbach.dhbw.i40_sensorVisualization.controller.Visualization", {

		_intervalID: null,

		/**
		 * Method for handling route pattern matched event if the view is displayed
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if the route pattern of the view matches
		 */
		_onObjectMatched: function(oEvent) {
			//specifies the sensor which should be visualized
			var sIndex = oEvent.getParameter("arguments").arrayIndex;
			var oSensor = this._getModel("sensor").getProperty("/sensors/" + sIndex);
			this._getModel("sensorDetail").setData(oSensor);
			//sets default values for the dateTimePicker in the diagram tab
			var oTimeNow = new Date();
			var oTimeFifteenMinBefore = new Date(oTimeNow.getTime() - 15 * 60 * 1000);
			this.getView().byId("dtpIntervalStart").setDateValue(oTimeFifteenMinBefore);
			this.getView().byId("dtpIntervalEnd").setDateValue(oTimeNow);
			//setting interval for querying sensor data
			this._byId("iconTabBar").setSelectedKey("monitorTab");
			if (this._intervalID === null && this._byId("iconTabBar").getSelectedKey() === "monitorTab") {
				this._getSensorData(this);
				this._intervalID = setInterval(this._getSensorData, this._getModel("sensorDetail").getProperty(
					"/refreshTime") * 1000, this);
			}
		},
		
		/**
		 * Method for restarting the querying interval
		 */
		restartInterval: function() {
			if (this._intervalID === null) {
				this._getSensorData(this);
				this._intervalID = setInterval(this._getSensorData, this._getModel("sensorDetail").getProperty("/refreshTime") * 1000, this);
			}
		},

		/**
		 * Method for clearing the querying interval
		 */
		clearInt: function(oController) {
			clearInterval(oController._intervalID);
			oController._intervalID = null;
		},

		/**
		 * Method for handling a press event of the icon tab filters of the tab bar
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if the route pattern of the view matches
		 */
		onTabSelected: function(oEvent) {
			var bCheckTresholdMail = this._getModel("sensorDetail").getProperty("/checkThresholdMail") === "true";
			var bCheckTresholdTwitter = this._getModel("sensorDetail").getProperty("/checkThresholdTwitter") === "true";
			//notifications not activated, though the querying interval can be cleared if the diagram tab is displayed 
			if (!bCheckTresholdMail && !bCheckTresholdTwitter) {
				switch (oEvent.getParameters("key").key) {
					case "monitorTab":
						this.restartInterval();
						break;
					case "diagramTab":
						this.clearInt(this);
						break;
					default:
				}
			}
		},

		/**
		 * Helper method for setting properties of a specified sensorUiModel (float, integer, boolean)
		 * @param {string} [sModel] name of the model
		 * @param {array} [aProperties] array of properties keys which should be set
		 * @param {obejct} [CalcValues] object of values to the corresponding property keys
		 */
		_setModelProperties: function(sModelName, aProperties, oCalcValues) {
			var oSensorUiModel = this._getModel(sModelName);
			jQuery.each(aProperties, function(nIndex, sProperty) {
				oSensorUiModel.setProperty("/" + sProperty, oCalcValues[sProperty]);
			});
		},

		/**
		 * Method for setting the value color of the specified tiles dependend on the displayed value
		 * @param {string} [sModel] name of the model
		 * @param {array} [aProperties] array of properties which should be checked
		 */
		_checkValues: function(sModelName, aProperties) {
			var oSensorDetailModel = this._getModel("sensorDetail");
			var nThresholdLow = Number(oSensorDetailModel.getProperty("/thresholdLow"));
			var nThresholdHigh = Number(oSensorDetailModel.getProperty("/thresholdHigh"));
			var oThis = this;
			jQuery.each(aProperties, function(nIndex, sProperty) {
				var sTileId = sProperty + "Tile";
				var nValue = Number(oThis._getModel(sModelName).getProperty("/" + sProperty));
				var nCritical = (nThresholdHigh - nThresholdLow) / 10;
				//value exceeds thresholds
				if (nValue > nThresholdHigh || nValue < nThresholdLow) {
					oThis.byId(sTileId).setValueColor("Error");
					return;
				}
				//value is in a critical area, but doesn't exceed thresholds
				if ((nThresholdHigh - nValue) < nCritical || (nValue - nThresholdLow) < nCritical) {
					oThis.byId(sTileId).setValueColor("Critical");
					return;
				}
				//everything is fine
				oThis.byId(sTileId).setValueColor("Good");
			});
		},

		/**
		 * Helper method for clearing the specified value tiles
		 * @param {array} [aValues] array of value tiles which should be cleared
		 */
		_resetTileContentColor: function(aValues) {
			var oThis = this;
			jQuery.each(aValues, function(nIndex, sValue) {
				var oTileContent = oThis.byId(sValue + "Tile");
				oTileContent.setValueColor("Neutral");
				oTileContent.setValue(0.0);
			});
		},

		/**
		 * Method for checking the thresholds and sending notifications if enabeled
		 * @param {object} [oMinMeasurement] object containing the value and timestamp of the minimal measurement of the query
		 * @param {object} [oMaxMeasurement] object containing the value and timestamp of the maximal measurement of the query
		 */
		_checkThreshold: function(oMinMeasurement, oMaxMeasurement) {
			var oSensorDetail = this._getModel("sensorDetail");
			var bCheckThresholdMail = (oSensorDetail.getProperty("/checkThresholdMail") === "true");
			var bCheckThresholdTwitter = (oSensorDetail.getProperty("/checkThresholdTwitter") === "true");
			var bThresholdExceeded = (oSensorDetail.getProperty("/thresholdExceeded") === "true");
			//email and/or twitter notification is enabled
			if (bCheckThresholdMail || bCheckThresholdTwitter) {
				var nMin = Number(oMinMeasurement.C_VALUE);
				var nMax = Number(oMaxMeasurement.C_VALUE);
				var nThresholdLow = Number(oSensorDetail.getProperty("/thresholdLow"));
				var nThresholdHigh = Number(oSensorDetail.getProperty("/thresholdHigh"));
				//threshold exceeded and last measurements were okay
				if ((nMin < nThresholdLow || nMax > nThresholdHigh) && !bThresholdExceeded) {
					//save critical state of the sensor
					this._updateSensor(true);
					//send email notification if enabled
					if (bCheckThresholdMail) {
						this._sendMail(oMinMeasurement, oMaxMeasurement, true);
					}
					//send twitter notification if enabled
					if (bCheckThresholdTwitter) {
						this._postTweet(oMinMeasurement, oMaxMeasurement, true);
					}
					return;
				}
				//measurements are okay and the sensor was in a critical state
				if (nMin >= nThresholdLow && nMax <= nThresholdHigh && bThresholdExceeded) {
					//save non critical state of the sensor 
					this._updateSensor(false);
					//send email notification if enabled
					if (bCheckThresholdMail) {
						this._sendMail(oMinMeasurement, oMaxMeasurement, false);
					}
					//send twitter notification if enabled
					if (bCheckThresholdTwitter) {
						this._postTweet(oMinMeasurement, oMaxMeasurement, false);
					}
				}
			}
		},

		/**
		 * Helper method for sending an email via the emailjs service
		 * @param {object} [oMinMeasurement] object containing the value and timestamp of the minimal measurement of the query
		 * @param {object} [oMaxMeasurement] object containing the value and timestamp of the maximal measurement of the query
		 * @param {boolean} [bExceeded] flag which indicates if the thresholds are exceeded or not
		 */
		_sendMail: function(oMinMeasurement, oMaxMeasurement, bExceeded) {
			var oData = this._buildMessage(oMinMeasurement, oMaxMeasurement);
			var sTemplate = bExceeded ? "thresholdexceededtrue" : "thresholdexceededfalse";
			emailjs.send("smtp_server", sTemplate, oData);
		},

		/**
		 * Helper method for building the email message
		 * @param {object} [oMinMeasurement] object containing the value and timestamp of the minimal measurement of the query
		 * @param {object} [oMaxMeasurement] object containing the value and timestamp of the maximal measurement of the query
		 * @return {object} the email message
		 */
		_buildMessage: function(oMinMeasurement, oMaxMeasurement) {
			var oSensorDetail = this._getModel("sensorDetail");
			return {
				receiver: oSensorDetail.getProperty("/email"),
				sensor: oSensorDetail.getProperty("/name"),
				machine: this._getMachineNameById(oSensorDetail.getProperty("/machine.machineID")),
				unit: oSensorDetail.getProperty("/unit"),
				thresholdLow: oSensorDetail.getProperty("/thresholdLow"),
				thresholdHigh: oSensorDetail.getProperty("/thresholdHigh"),
				timestampMin: sap.ui.core.format.DateFormat.getDateTimeInstance({
					style: "long"
				}).format(oMinMeasurement.C_TIMESTAMP),
				minValue: Number(oMinMeasurement.C_VALUE),
				timestampMax: sap.ui.core.format.DateFormat.getDateTimeInstance({
					style: "long"
				}).format(oMaxMeasurement.C_TIMESTAMP),
				maxValue: Number(oMaxMeasurement.C_VALUE)
			};
		},

		/**
		 * Helper method for making a tweet via the codebird-API
		 * @param {object} [oMinMeasurement] object containing the value and timestamp of the minimal measurement of the query
		 * @param {object} [oMaxMeasurement] object containing the value and timestamp of the maximal measurement of the query
		 * @param {boolean} [bExceeded] flag which indicates if the thresholds are exceeded or not
		 */
		_postTweet: function(oMinMeasurement, oMaxMeasurement, bExceeded) {
			var oData = this._buildMessage(oMinMeasurement, oMaxMeasurement);
			var timestamp = sap.ui.core.format.DateFormat.getDateTimeInstance({
							style: "long"}).format(new Date());
			var params = {
				status: ""
			};
			if (bExceeded) {
				params.status = oData.machine + " (" + oData.sensor + ") am " + timestamp + " im kritischen Zustand! #hilfe";
			} else {
				params.status = oData.machine + " (" + oData.sensor + ") am " + timestamp + " im Sollzustand! #alleswirdgut";
			}
			cb.__call(
				"statuses_update",
				params,
				function(reply) {
				}
			);
		},
		
		/**
		 * Helper method for updating the sensor state (critical or non-critical)
		 * @param {boolean} [bExceeded] flag which indicates if the state is critical or not
		 */
		_updateSensor: function(bExceeded) {
			var sExceeded = bExceeded ? "true" : "false";
			this._getModel("sensorDetail").setProperty("/thresholdExceeded", sExceeded);
			var oSensor = this._getModel("sensorDetail").getProperty("/");
			var oThis = this;
			jQuery.ajax({
				type: "PUT",
				contentType: "application/json",
				url: "/rest/sensor",
				data: JSON.stringify(oSensor),
				dataType: "json",
				error: function(oError) {
					oThis._createDialog("titleXS", "messageXS");
				},
				success: function(oData) {}
			});
		},
		
		/**
		 * Method for displaying the history of sensor data in a specified type of diagram
		 * @param {string} [sDiagramType] the type of the diagram
		 * @return {boolean} [bIsBoolSensor] flag indicates if it's a boolean sensor
		 */
		_onShowData: function(sDiagramType, bIsBoolSensor) {
			var oSensorDetailModel = this._getModel("sensorDetail");
			var sTechnicalSensorName = oSensorDetailModel.getProperty("/technicalName");
			var oController = this;
			var oIntervalStart = this.getView().byId("dtpIntervalStart").getDateValue();
			var oIntervalEnd = this.getView().byId("dtpIntervalEnd").getDateValue();
			//checks if time interval is invalid
			if (oIntervalStart > oIntervalEnd || oIntervalStart === null || oIntervalEnd === null) {
				this._createDialog("wrongTimeInterval", "wrongTimeIntervalMessage");
				return;
			}
			var oModel = this.getView().getModel("odata");
			var aDataPoints = [];
			//querying data for the specfied time interval
			oModel.read(this._getModel("config").getProperty("/tableName"), {
				filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", oIntervalStart, oIntervalEnd), new sap.ui.model.Filter("C_SENSORNAME",
					"EQ",
					sTechnicalSensorName)],
				sorters: [new sap.ui.model.Sorter("C_TIMESTAMP", false)],
				success: function(oData) {
					var nResultsLength = oData.results.length;
					var nDataPoints = oController.getView().byId("dataPointsInput").getValue();
					if (bIsBoolSensor) {
						//replacing strings ("false" or "true") by the integers 0 or 1
						jQuery.each(oData.results, function(nIndex, oDataPoint) {
							oDataPoint.C_VALUE = (oDataPoint.C_VALUE === "True" ? 1 : 0);
						});
					}
					//adapting the data set to the maximum number of data points specified by the user if necessary
					if (nResultsLength > nDataPoints) {
						var nStepsize = Math.ceil(nResultsLength / nDataPoints);
						jQuery.each(oData.results, function(nIndex, oDataPoint) {
							if (nIndex % nStepsize === 0) {
								aDataPoints.push(oDataPoint);
							}
						});
					} else {
						aDataPoints = oData.results;
					}
					oController._createVizFrame(aDataPoints, sDiagramType);
				}
			});
		},
		
		/**
		 * Helper method for creating the diagram
		 * @param {array} [aDataPoints] array of data points to be visualized
		 * @return {boolean} [sDiagramType] type of the diagram ("line" or "column")
		 */
		_createVizFrame: function(aDataPoints, sDiagramType) {
			var oResourceBundle = this._getResourceBundle();
			var sUnit = this._getModel("sensorDetail").getProperty("/unit");
			//sensor is of type boolean
			if (this._getModel("sensorDetail").getProperty("/sensorType") === "b") {
				sUnit = oResourceBundle.getText("state");
			}
			var oVizFrame = this.getView().byId("idcolumn");
			oVizFrame.destroyDataset();
			oVizFrame.destroyFeeds();
			var oModel = new sap.ui.model.json.JSONModel({
				data: aDataPoints
			});
			this._setModel(oModel, "visual");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: oResourceBundle.getText("time"),
					value: {
						path: "C_TIMESTAMP",
						formatter: function(sValue) {
							var timeFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
								pattern: "dd.MM. HH:mm:ss"
							});
							return timeFormat.format(sValue);
						}
					}
				}],
				measures: [{
					name: sUnit,
					value: "{C_VALUE}"
				}],
				data: "{visual>/data}"
			});
			oVizFrame.setDataset(oDataset);
			oVizFrame.setModel(oModel);
			oVizFrame.setVizType(sDiagramType);
			oVizFrame.setVizProperties({
				plotArea: {
					colorPalette: d3.scale.category20().range()
				},
				title: {
					text: oResourceBundle.getText("history")
				}
			});
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "valueAxis",
				'type': "Measure",
				'values': [sUnit]
			});
			var feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "categoryAxis",
				'type': "Dimension",
				'values': [oResourceBundle.getText("time")]
			});
			oVizFrame.addFeed(feedValueAxis);
			oVizFrame.addFeed(feedCategoryAxis);
		}
	});
});