sap.ui.define([
	"mosbach/dhbw/i40_sensorVisualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("mosbach.dhbw.i40_sensorVisualization.controller.MainMenu", {

		_bSettingsTileAdded: null,

		/**
		 * Method for initializing the controller's attributes
		 * and loading the active machines for displaying
		 */
		onInit: function() {
			this._bSettingsTileAdded = false;
			var oThis = this;
			jQuery.ajax({
				type: "GET",
				contentType: "application/json",
				url: "/rest/machine",
				dataType: "json",
				error: function(oError) {
					oThis._createDialog("titleXS", "messageXS");
				},
				success: function(oData) {
					oThis._getModel("machine").setData({
						machines: oData
					});
				}
			});
		},
		
		/**
		 * Method for dynamically adding the settings tile to the view
		 */
		onAfterRendering: function() {
			if (!this._bSettingsTileAdded) {
				this._bSettingsTileAdded = true;
				var oTileContainer = this.byId("tileContainer");
				oTileContainer.addTile(new sap.m.StandardTile({
					title: this._getResourceBundle().getText("settings"),
					icon: "sap-icon://settings",
					press: [{
						controller: this
					}, this.onSettingsTilePressed]
				}));
			}
		},
		
		/**
		 * Method for handling press events on a machine tile
		 * @param {sap.ui.base.Event} [oEvent] event which is fired by pressing a machine tile
		 */
		onTilePressed: function(oEvent) {
			var sPath = oEvent.getSource().getBindingContext("machine").getPath();
			var oMachine = this._getModel("machine").getProperty(sPath);
			//set the machine id as a routing parameter and navigate to sensor view
			this._getRouter().navTo("sensor", {
				machineID: oMachine.machineID
			});
		},

		/**
		 * Method for handling press events on the settings tile
		 * @param {sap.ui.base.Event} [oEvent] event which is fired by pressing the settings tile
		 * @param {object} [oData] object which gives access to the current controller
		 */
		onSettingsTilePressed: function(oEvent, oData) {
			oData.controller._getRouter().navTo("settings");
		}
	});
});