sap.ui.define([
	"mosbach/dhbw/i40_sensorVisualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("mosbach.dhbw.i40_sensorVisualization.controller.Sensor", {

		/**
		 * Method for initializing the controller
		 */
		onInit: function() {
			this._getRouter().getRoute("sensor").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * Method for handling route pattern matched event if the view is displayed
		 * @param {sap.ui.base.Event} [oEvent] event which is fired if the route pattern of the view matches
		 */
		_onObjectMatched: function(oEvent) {
			var machineID = parseInt(oEvent.getParameter("arguments").machineID, 10);
			this._byId("sensorPage").setTitle(this._getMachineNameById(machineID));
			var oThis = this;
			//read all sensors of machine by setting the machine id as a url parameter
			jQuery.ajax({
				type: "GET",
				contentType: "application/json",
				url: "/rest/sensor",
				data: {
					machineID: machineID
				},
				dataType: "json",
				error: function(oError) {
					oThis._createDialog("titleXS", "messageXS");
				},
				success: function(oData) {
					oThis._getModel("sensor").setData({
						sensors: oData
					});
				}
			});
		},

		/**
		 * Method for handling press events on the sensor tiles
		 * @param {sap.ui.base.Event} [oEvent] event which is fired by pressing a sensor tile
		 */
		onTilePressed: function(oEvent) {
			var sPath = oEvent.getSource().getBindingContext("sensor").getPath();
			var oSensor = this._getModel("sensor").getProperty(sPath);
			//navigate to view dependend on the type of the sensor (float, int or boolean)
			switch (oSensor.sensorType) {
				case "f":
					this._getRouter().navTo("visualizationFloat", {
						arrayIndex: sPath.split("/")[2]
					});
					break;
				case "i":
					this._getRouter().navTo("visualizationInt", {
						arrayIndex: sPath.split("/")[2]
					});
					break;
				case "b":
					this._getRouter().navTo("visualizationBool", {
						arrayIndex: sPath.split("/")[2]
					});
					break;
				default:
					this._createDialog("titleWrongSensorType", "messageWrongSensorType");
			}
		}
	});
});