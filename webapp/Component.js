sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/core/routing/History"
], function(UIComponent, History) {
	"use strict";

	return UIComponent.extend("mosbach.dhbw.i40_sensorVisualization.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			//initialize the router 
			this.getRouter().initialize();
			this.getRouter().attachRouteMatched(this._clearInt, this);

		},

		_clearInt: function(oEvent) {

			var sRouteName = oEvent.getParameter("name");
			var aViews = [];
			aViews.push(sap.ui.getCore().byId(this.createId("visualizationFloat")));
			aViews.push(sap.ui.getCore().byId(this.createId("visualizationInt")));
			aViews.push(sap.ui.getCore().byId(this.createId("visualizationBool")));

			jQuery.each(aViews, function(nIndex, oView) {

				if (!jQuery.sap.startsWithIgnoreCase(sRouteName, "visualization")) {
					if (oView !== undefined) {
						var oVizViewController = oView.getController();
						oVizViewController.clearInt(oVizViewController);
					}
				}
			});
		}
	});
});